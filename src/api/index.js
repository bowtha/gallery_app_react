import axios from 'axios'

export const host = "https://gallery-wonderland-api.twilightparadox.com"

// Get All Image
export function getAllImage(token) {
    return axios.get(`${host}/allimage`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getAllImageSort(token) {
    return axios.get(`${host}/allimagesort`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}


// Get All Gallery By UserID
export function getAllGallery(token,user_id) {
    return axios.get(`${host}/allgallery/${user_id}`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getAllGallerySort(token,user_id) {
    console.log('sort ----> user_id',user_id)
    return axios.get(`${host}/allgallerysort/${user_id}`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

//Get User by search name
export function searchUserByname(token,search) {
    return axios.get(`${host}/search?search=${search}`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}



// Get Gallery By GalleryID
export function getGallery(token,gallery_id) {
    return axios.get(`${host}/gallery/${gallery_id}`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}
// Post Gallery
export function postGallery(token,gallery) {
    return axios.post(`${host}/gallery`,gallery,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}
// Get All Image in Gallery By GalleryID
export function getImageInGallery(token,gallery_id) {
    return axios.get(`${host}/imagelist/${gallery_id}`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function postImage(token,image) {
    return axios.post(`${host}/image/${image.GalleryID}`, image,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}


export function deleteGallery(token,gallery_id) {
    return axios.delete(`${host}/gallery/${gallery_id}`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function deleteAllImage(token,gallery_id) {
    return axios.delete(`${host}/allimage/${gallery_id}`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}


// Delete image by ID
export function deleteImage(token,image_id) {
    return axios.delete(`${host}/image/${image_id}`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function updateProfileImage(token,user_id,fileImg){
    return axios.patch(`${host}/user/${user_id}`,fileImg,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function updateNameGallery(token,gallery_id,name){
    return axios.patch(`${host}/namegallery/${gallery_id}`,name,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function updateCoverGallery(token,gallery_id,file){
    return axios.patch(`${host}/covergallery/${gallery_id}`,file,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function updateStatusGallery(token,gallery_id, status){
    return axios.patch(`${host}/statusgallery/${gallery_id}`, {
        status: status,
    }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function login(data) {
    return axios.post(`${host}/login`, data)
}


export function signup(data) {
    return axios.post(`${host}/signup`, data)
}

export function getUsersByToken(token) {
    return axios.get(`${host}/user`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getUserDataById(token,user_id) {
    return axios.get(`${host}/user/${user_id}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}


export function getGalleryByUserId(token,user_id) {
    return axios.get(`${host}/gallerybyuser/${user_id}`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function postLike(token,image_id,user_id) {
    return axios.post(`${host}/like/${image_id}/${user_id}`,{},{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function logout(token,user_id) {
    return axios.patch(`${host}/logout/${user_id}`,{},{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getLikeById(token,image_id) {
    return axios.get(`${host}/like/${image_id}`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}


