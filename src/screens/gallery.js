import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import { Container, Row, Col, Input, Label } from 'reactstrap';
import { Switch, Modal, Upload ,Spin } from 'antd';
import 'antd/dist/antd.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUpload } from '@fortawesome/free-solid-svg-icons'
import { getImageInGallery, postImage, getGallery, updateCoverGallery, updateNameGallery, updateStatusGallery, deleteGallery, deleteAllImage, deleteImage, getUsersByToken, getGalleryByUserId, searchUserByname, host } from '../api/index'
import ImgCrop from 'antd-img-crop';
import logo from '../wonderland.png'
import Button from '@material-ui/core/Button';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import SaveIcon from '@material-ui/icons/Save';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import { green } from '@material-ui/core/colors';
import TextFieldsIcon from '@material-ui/icons/TextFields';
import ImageIcon from '@material-ui/icons/Image';
import PublicIcon from '@material-ui/icons/Public';


const Gallery = (props) => {

    const [open, setOpen] = useState(false);
    const [hidden, setHidden] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const token = useSelector(state => state.login.token)
    const gallery_id = props.match.params.id

    const [cando, setCanDo] = useState(false)
    const [user, setUser] = useState({})

    const [friends, setFriends] = useState([])
    const [search, setSearch] = useState("")

    const [visible, setVisible] = useState(false)
    const [confirmDelete, setConfirmDelete] = useState(false)
    const [visibleGalleryName, setVisibleGalleryName] = useState(false)
    const [visibleGalleryCover, setVisibleGalleryCover] = useState(false)
    const [visibleGalleryStatus, setVisibleGalleryStatus] = useState(false)

    const [gallery, setGallery] = useState([])
    const [header, setHeader] = useState([])
    const [Image, setImage] = useState([])


    const [Name, setName] = useState(null)
    const [Status, setStatus] = useState(null)
    const [Cover, setCover] = useState(null)
    const [listGallery, setListGallery] = useState([])

    const [edit, setEdit] = useState(false)
    const [editGalleryName, setEditGalleryName] = useState("")
    const [confirmDeleteImage, setConfirmDeleteImage] = useState(false)
    const [IDForDeleteImage, setIDForDeleteImage] = useState(null)

    // Get Gallery Details
    const fetchGallery = () => {
        getGallery(token, gallery_id)
            .then(response => fetchHeader(response.data))
            .catch(err => console.log(err))
    }

    //filter Details Gallery by id(Gallery)
    const fetchHeader = (data) => {
        setListGallery(data)
        const id = props.match.params.id
        const header = data.filter(item => item.ID == id)
        setHeader(header)
    }

    //Get All Image in Gallery
    const fetchImage = () => {
        getImageInGallery(token, gallery_id)
            .then(response => setGallery(response.data))
            .catch(err => console.log(err))
    }

    //set data for add image in Gallery
    const handleOk = () => {
        if (Image == null) {
            alert("Please upload image")
        }
        else if (Name == "") {
            alert("Please input image name")
        }
        else if (Image == null && Name == "") {
            alert("Please input image name and upload image")
        }
        else {
            var file = new File([Image], `${Image.lastModifiedDate}.png`);
            const formData = new FormData()
            formData.append("Name", Name)
            formData.append("Image", file)
            formData.append("GalleryID", gallery_id)

            createImage(formData)
            setVisible(false)
        }
    };

    const Search = (value) => {
        if (value == "") {
            setFriends([])
        }
        else {
            searchUserByname(token, value)
                .then(response => {
                    console.log("search--->", response.data)
                    setFriends(response.data)
                })
                .catch(err => console.log(err))
        }
    }

    // Create Image in Gallery
    const createImage = (formData) => {
        postImage(token, formData)
            .then(() => window.location = "/gallery/" + gallery_id)
            .catch(err => console.log(err))
    }

    //Cancel All modal
    const handleCancel = () => {
        setVisible(false)
        setConfirmDelete(false)
        setConfirmDeleteImage(false)
        setVisibleGalleryCover(false)
        setVisibleGalleryName(false)
        setVisibleGalleryStatus(false)
    };

    //Update Gallery
    const handleOkChangeGalleryName = () => {
        if (editGalleryName == "") {
            alert("please input new name gallery")
        }
        else {
            const formData = new FormData()
            formData.append("Name", editGalleryName)
            updateNameGallery(token, gallery_id, formData)
                .then(() => { setEdit(false) })
                .then(() => window.location.reload())
                .catch(err => console.log(err))
        }

    }

    const handleOkChangeGalleryCover = () => {
        if (Cover == null) {
            alert("please upload new cover")
        }
        else {
            var file = new File([Cover], `${Cover.lastModifiedDate}.png`);
            const formData = new FormData()
            formData.append("Cover", file)
            updateCoverGallery(token, gallery_id, formData)
                .then(() => { setEdit(false) })
                .then(() => window.location.reload())
                .catch(err => console.log(err))
        }

    }

    const handleOkChangeGalleryStaus = () => {

        console.log('Status', Status)
        console.log('Token', token)
        updateStatusGallery(token, gallery_id, Status)
            .then(() => { setEdit(false) })
            .then(() => window.location.reload())
            .catch(err => console.log(err))
    }


    // confirm delete image
    const handleOkconfirmDeleteImage = () => {
        setConfirmDeleteImage(false)
        deleteImage(token, IDForDeleteImage)
            .then(() => fetchImage())
            .catch(err => console.log(err))
    }
    // confirm ok (delete image)
    const handleOkconfirmDelete = (gallery) => {
        setConfirmDelete(false)
        deleteThisGallery(gallery)
    }
    // set id for delete image
    const wantedTodelete = (id) => {
        setIDForDeleteImage(id)
        setConfirmDeleteImage(true)
    }

    // Delete Gallery (gallery and image in gallery)
    const deleteThisGallery = (gallery) => {
        deleteGallery(token, gallery.ID)
            .catch(err => console.log(err))
        deleteAllImage(token, gallery.ID)
            .then(() => window.location = `/home/${user.ID}`)
            .catch(err => console.log(err))
    }

    const fetchUser = () => {
        getUsersByToken(token)
            .then((response) => {
                setUser(response.data)
                fetchGalleryForCheck(response.data.ID)
            })
            .catch(err => console.log(err))
    }

    const fetchGalleryForCheck = (user_id) => {
        getGalleryByUserId(token, user_id)
            .then(response => {
                checkStatusCanDo(response.data)
            })
            .catch(err => console.log(err))
    }

    const checkStatusCanDo = (galleryList) => {
        console.log('galleryList', galleryList)
        galleryList.find(item => {
            if (item.ID == gallery_id) {
                setCanDo(true)
            }
        })

    }

    useEffect(() => {
        fetchUser()
        fetchGallery()
        fetchImage()
    }, [])


    const actions = [
        { icon: <TextFieldsIcon />, name: 'Gallery Name', func: () => setVisibleGalleryName(true) },
        { icon: <ImageIcon />, name: 'Gallery Cover', func: () => setVisibleGalleryCover(true) },
        { icon: <PublicIcon/>, name: 'Gallery Status', func: () => setVisibleGalleryStatus(true) },
    ];

    return (

        <div>
            {
                user == {} || Image == [] || gallery == [] ?
                    <Spin />
                    :
                    < Container style={{ height: '72px', alignItems: 'center' }}>
                        <Row xs="3" >
                            <Col style={{ alignSelf: 'center', paddingTop: 24, paddingBottom: 24, alignItems: 'left' }}>
                                <img
                                    onClick={() => window.location = "/list"}
                                    src={logo}
                                    width="250" height="80" />
                            </Col>
                            <Col style={{ alignSelf: 'center', paddingTop: 24, textAlign: 'center', paddingBottom: 24 }}>
                                <Input
                                    style={{ width: '50%', marginLeft: '25%' }}
                                    placeholder="ค้นหา"
                                    onChange={(e) => Search(e.target.value)}
                                    valur={search}
                                />
                                <div style={{ position: 'absolute', width: '50%', zIndex: 1, marginLeft: '23%' }}>
                                    {
                                        friends == [] && search == "" ?
                                            null
                                            :
                                            friends.map(f => {
                                                return (
                                                    <Row
                                                        onClick={() => window.location = `/home/${f.ID}`}
                                                        style={{ marginTop: '5%', width: '90%', marginLeft: '1%', backgroundColor: '#8080807d', borderRadius: 3, padding: 3, alignItems: 'center', paddingLeft: 5 }}>
                                                        <img
                                                            src={`${host}/images/${f.Profile}`}
                                                            style={{ width: 30, height: 30, borderRadius: 50 }} />
                                                        <p style={{ marginLeft: '10%', marginTop: '5%' }}>{f.Email}</p>
                                                    </Row>
                                                )
                                            })
                                    }
                                </div>
                            </Col>
                            <Col style={{ paddingRight: '10%', textAlign: 'right', alignSelf: 'center', paddingTop: 24, paddingBottom: 24 }}>
                                <img
                                    onClick={() => window.location = `/home/${user.ID}`}
                                    src={`${host}/images/${user.Profile}`}
                                    style={{ width: 50, height: 50, borderRadius: 50, marginLeft: '100%' }}></img>
                            </Col>
                        </Row>
                        <hr style={{ marginTop: 0 }} />
                        {
                            cando == false ?
                                null
                                :
                                edit == true ?
                                    <Row style={{ justifyContent: 'center' ,marginBottom:'3%',marginTop:'1%'}}>
                                        <Button
                                            onClick={() => setEdit(false)}
                                            variant="contained"
                                            style={{ color: green[500] }}
                                            style={{ marginRight: 50, marginLeft: 50 }}
                                            startIcon={<SaveIcon />}
                                        >Edit Success</Button>
                                    </Row>
                                    :
                                    <Row style={{ marginLeft: '3%' }}>
                                        <Col style={{ width: '20%', marginTop: '2%', marginBottom: '3%', textAlign: 'center' }}>
                                            <Button
                                                variant="contained"
                                                color="primary"
                                                startIcon={<AddIcon />}
                                                onClick={() => setVisible(true)}
                                            >Add Image</Button>
                                            <Button
                                                onClick={() => setEdit(true)}
                                                variant="contained"
                                                style={{ color: green[500] }}
                                                style={{ marginRight: 50, marginLeft: 50 }}
                                                startIcon={<EditIcon />}
                                            >Edit Gallery</Button>
                                            <Button
                                                variant="contained"
                                                color="secondary"
                                                startIcon={<DeleteIcon />}
                                                onClick={() => setConfirmDelete(true)}
                                            >Delete Gallery</Button>
                                        </Col>
                                    </Row>
                        }
                        {
                            header == [] ?
                                null
                                :
                                header.map(item => {
                                    return (
                                        <Row style={{ backgroundImage: `url(${host}/images/${item.Cover})`, backgroundRepeat: 'no-repeat', backgroundPosition: 'center', opacity: 0.8, borderRadius: 30, backgroundSize: 'cover' }}>
                                            <Col style={{ paddingTop: 24, paddingBottom: 24, height: 300 }}>
                                                <div style={{ width: '60%', backgroundColor: '#faebd7bf', borderRadius: 20, padding: 30, marginLeft: '20%', marginTop: '7%', textAlign: 'center' }}>
                                                    <h2>{item.Name}</h2>
                                                </div>
                                            </Col>
                                        </Row>
                                    )
                                })
                        }

                        {
                            cando == false ?
                                null
                                :
                                edit == false ?
                                    null
                                    :
                                    <Row style={{ justifyContent: 'flex-end', marginTop: '-13%' }}>
                                        <SpeedDial
                                            ariaLabel="SpeedDial"
                                            hidden={hidden}
                                            icon={<EditIcon openIcon={<EditIcon />} />}
                                            onClose={handleClose}
                                            onOpen={handleOpen}
                                            open={open}
                                        >
                                            {actions.map((action) => (
                                                <SpeedDialAction
                                                    key={action.name}
                                                    icon={action.icon}
                                                    tooltipTitle={action.name}
                                                    onClick={action.func}
                                                />
                                            ))}
                                        </SpeedDial>
                                    </Row>
                        }



                        <Row Row xs="3" style={{ marginTop: 30 }}>
                            {
                                gallery == [] ?
                                    null
                                    :
                                    gallery.map((item, index) => {
                                        return (
                                            <Col style={{ textAlign: 'center' }} key={item.ID}>
                                                <Row>
                                                    <img
                                                        src={`${host}/images/${item.Image}`}
                                                        style={{ width: '90%', height: '90%', margin: '5%', borderRadius: 30 }}
                                                    // onClick={}
                                                    />
                                                    {
                                                        edit == false ?
                                                            null
                                                            :
                                                            <Button
                                                            variant="contained"
                                                            color="secondary"
                                                            style={{  marginTop: '-100%', marginLeft: '85%', height: 50 ,paddingLeft:23}} 
                                                            startIcon={<DeleteIcon style={{fontSize:20,textAlign:'center'}}/>}
                                                            onClick={() => wantedTodelete(item.ID)}>
                                                       </Button>
                                                    
                                                    }
                                                </Row>
                                                <Row>
                                                    <Col style={{ textAlign: 'center' }}><h5>{item.Name}</h5></Col>
                                                </Row>

                                            </Col>

                                        )
                                    })
                            }

                        </Row>
                        {/* <Row>
                            
                        </Row> */}
                        <Modal
                            title="Add Gallery"
                            visible={visible}
                            onOk={handleOk}
                            onCancel={handleCancel}
                        >
                            <Row>
                                <Col style={{ flex: 1, width: '50%', marginLeft: '25%', marginRight: '25%', flexDirection: 'row' }}>
                                    <Row>
                                        <Label for="name">Name</Label>
                                        <Input type="text" name="name" id="name" placeholder="name" onChange={(e) => setName(e.target.value)} />
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Row style={{ marginTop: 10 }}>
                                                <Label for="coverGallery">Image</Label>
                                            </Row>
                                            <Row>
                                                <ImgCrop>
                                                    <Upload
                                                        name="coverGallery" listType="picture"
                                                        beforeUpload={(blob) => setImage(blob)}
                                                    >
                                                        <Button><FontAwesomeIcon icon={faUpload} style={{ marginRight: 5 }} />Click to upload</Button>
                                                    </Upload>
                                                </ImgCrop>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Modal>
                        <Modal
                            title="Confirm Delete Gallery"
                            visible={confirmDelete}
                            onOk={() => handleOkconfirmDelete(header[0])}
                            onCancel={handleCancel}
                        >
                            <Row>
                                <Col style={{ flex: 1, width: '50%', marginLeft: '25%', marginRight: '25%', flexDirection: 'row' }}>
                                    <Row>
                                        <Label >You want to delete Gallery ?</Label>
                                    </Row>
                                </Col>
                            </Row>
                        </Modal>
                        <Modal
                            title="Confirm Delete Image"
                            visible={confirmDeleteImage}
                            onOk={handleOkconfirmDeleteImage}
                            onCancel={handleCancel}
                        >
                            <Row>
                                <Col style={{ flex: 1, width: '50%', marginLeft: '25%', marginRight: '25%', flexDirection: 'row' }}>
                                    <Row>
                                        <Label >You want to delete Image ?</Label>
                                    </Row>
                                </Col>
                            </Row>
                        </Modal>
                        <Modal
                            title="Change Gallery Cover"
                            visible={visibleGalleryCover}
                            onOk={handleOkChangeGalleryCover}
                            onCancel={handleCancel}
                        >
                            <Row>
                                <Col style={{ flex: 1, width: '50%', marginLeft: '25%', marginRight: '25%', flexDirection: 'row' }}>
                                    <ImgCrop>
                                        <Upload
                                            name="coverGallery" listType="picture"
                                            beforeUpload={(blob) => setCover(blob)}
                                        >
                                            <Button><FontAwesomeIcon icon={faUpload} style={{ marginRight: 5 }} />Click to upload</Button>
                                        </Upload>
                                    </ImgCrop>
                                </Col>
                            </Row>
                        </Modal>
                        <Modal
                            title="Change Gallery Name"
                            visible={visibleGalleryName}
                            onOk={handleOkChangeGalleryName}
                            onCancel={handleCancel}
                        >
                            <Row>
                                <Col style={{ flex: 1, width: '50%', marginLeft: '25%', marginRight: '25%', flexDirection: 'row' }}>
                                    <Row>
                                        <Label>Name</Label>
                                        <Input type="text" placeholder="Gallery Name" onChange={(e) => setEditGalleryName(e.target.value)} />
                                    </Row>
                                </Col>
                            </Row>
                        </Modal>
                        <Modal
                            title="Change Gallery Status"
                            visible={visibleGalleryStatus}
                            onOk={handleOkChangeGalleryStaus}
                            onCancel={handleCancel}
                        >
                            <Row>
                                <Col style={{ flex: 1, width: '50%', marginLeft: '25%', marginRight: '25%', flexDirection: 'row' }}>
                                    <Row>
                                        <Label>Status</Label>
                                        {
                                            header.map(i => {
                                                if (i.Status == false) {
                                                    return (
                                                        <Switch checkedChildren="publice" unCheckedChildren="private" onChange={(e) => setStatus(e)} style={{ width: 80, marginLeft: '5%' }} />

                                                    )
                                                }
                                                else {
                                                    return (
                                                        <Switch checkedChildren="publice" unCheckedChildren="private" defaultChecked onChange={(e) => setStatus(e)} style={{ width: 80, marginLeft: '5%' }} />
                                                    )
                                                }
                                            })
                                        }
                                    </Row>
                                </Col>
                            </Row>
                        </Modal>
                    </Container>

            }

        </div >
    );
};
export default Gallery