import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import { BackTop } from 'antd'
import { Container, Row, Col, Input, Label } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ArrowUpOutlined, ArrowDownOutlined, UpCircleFilled, FilterTwoTone } from '@ant-design/icons';
import { Button ,ButtonGroup} from '@material-ui/core';

import 'antd/dist/antd.css';
import logo from '../wonderland.png'
import { getAllImage, getUsersByToken, searchUserByname, host, getAllImageSort } from '../api/index'


const AllImage = () => {


    const token = useSelector(state => state.login.token)
    const [listImage, setListImage] = useState([])
    const [user, setUser] = useState({})

    const [friends, setFriends] = useState([])
    const [search, setSearch] = useState("")
    const [sort, setSort] = useState(false)


    const fetchAllimage = () => {
        setSort(!sort)
        getAllImage(token)
            .then((response) => setListImage(response.data))
            .catch(err => console.log(err))
    }

    const fetchAllimageSort = () => {
        setSort(!sort)
        getAllImageSort(token)
            .then((response) => setListImage(response.data))
            .catch(err => console.log(err))
    }



    const fetchUser = () => {
        getUsersByToken(token)
            .then((response) => {
                console.log("user", response.data.Profile)
                setUser(response.data)
            })
            .catch(err => console.log(err))
    }

    const Search = (value) => {
        if (value == "") {
            setFriends([])
        }
        else {
            searchUserByname(token, value)
                .then(response => {
                    console.log("search--->", response.data)
                    setFriends(response.data)
                })
                .catch(err => console.log(err))
        }

    }

    useEffect(() => {
        fetchAllimage()
        fetchUser()
    }, [])

    return (
        <div>
            {
                listImage == [] && user == {} ?
                    null
                    :
                    <Container style={{ height: '72px', alignItems: 'center' }}>
                        <Row xs="3" >
                            <Col style={{ alignSelf: 'center', paddingTop: 24, paddingBottom: 24, alignItems: 'left' }}>
                                <img
                                    onClick={() => window.location = "/list"}
                                    src={logo}
                                    width="250" height="80" />
                            </Col>
                            <Col style={{ alignSelf: 'center', paddingTop: 24, textAlign: 'center', paddingBottom: 24 }}>
                                <Input
                                    style={{ width: '50%', marginLeft: '25%' }}
                                    placeholder="ค้นหา"
                                    onChange={(e) => Search(e.target.value)}
                                    valur={search}
                                />
                                <div style={{ position: 'absolute', width: '50%', zIndex: 1, marginLeft: '23%' }}>
                                    {
                                        friends == [] && search == "" ?
                                            null
                                            :
                                            friends.map(f => {
                                                return (
                                                    <Row
                                                        onClick={() => window.location = `/home/${f.ID}`}
                                                        style={{ marginTop: '5%', width: '90%', marginLeft: '1%', backgroundColor: '#8080807d', borderRadius: 3, padding: 3, alignItems: 'center', paddingLeft: 5 }}>
                                                        <img
                                                            src={`${host}/images/${f.Profile}`}
                                                            style={{ width: 30, height: 30, borderRadius: 50 }}></img>
                                                        <p style={{ marginLeft: '10%', marginTop: '5%' }}>{f.Email}</p>
                                                    </Row>
                                                )
                                            })
                                    }
                                </div>
                            </Col>
                            <Col style={{ paddingRight: '10%', textAlign: 'right', alignSelf: 'center', paddingTop: 24, paddingBottom: 24 }}>
                                <img
                                    onClick={() => window.location = `/home/${user.ID}`}
                                    src={`${host}/images/${user.Profile}`}
                                    style={{ width: 50, height: 50, borderRadius: 50, marginLeft: '100%' }}></img>
                            </Col>
                        </Row>
                        <Row style={{marginLeft:'1%'}}>
                            <ButtonGroup disableElevation variant="contained" color="secondary">
                                <Button onClick={() => fetchAllimageSort()}  disabled={!sort}>newest</Button>
                                <Button onClick={() => fetchAllimage()} disabled={sort}>oldest</Button>
                            </ButtonGroup>
                        </Row>
                        <Row xs="1">
                            {
                                listImage.map(item => {
                                    // fetchLikes(item.ID)
                                    return (
                                        <div style={{ background: 'transparent' }}>
                                            <Row>
                                                <img
                                                    src={`${host}/images/${item.UserProfile}`}
                                                    style={{ width: 50, height: 50, marginTop: '5%', marginLeft: '21%', borderRadius: 50 }}
                                                    onClick={() => window.location = "/home/" + item.UserID}
                                                />
                                                <p style={{ marginTop: '6%', marginLeft: '3%', fontSize: 'initial' }}>{item.UserEmail}</p>
                                            </Row>
                                            <img
                                                src={`${host}/images/${item.Image}`}
                                                style={{ width: '60%', height: '80%', marginTop: '2%', marginLeft: '20%' }}
                                                onClick={() => window.location = "/gallery/" + item.GalleryID}
                                            />

                                        </div>
                                    )

                                })
                            }
                        </Row>
                        <BackTop style={{}}>
                            <UpCircleFilled spin style={{ width: 50, height: 50, fontSize: 32 }} />
                        </BackTop>
                    </Container>

            }

        </div>
    )

}
export default AllImage