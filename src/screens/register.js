import React, { useState, useEffect } from 'react';
import { Input, Label, Col, Row, Alert } from 'reactstrap';
import { signup } from '../api';
import { Upload } from 'antd';
import { TextField, Button } from '@material-ui/core';
import 'antd/dist/antd.css';
import ImgCrop from 'antd-img-crop';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUpload } from '@fortawesome/free-solid-svg-icons'
import regis from '../regis.png'
import logo from '../wonderland.png'
import CloudUploadIcon from '@material-ui/icons/CloudUpload';


const Register = () => {

    const [Email, setEmail] = useState("")
    const [Profile, setProfile] = useState([])
    const [ProfileBlob, setProfileBlob] = useState([])
    const [Password, setPassword] = useState("")
    const [emailErr, setEmailErr] = useState(false)
    const [passwordErr, setPasswordErr] = useState(false)
    const [profileErr, setProfileErr] = useState(false)

    const Register = () => {
        if (Email == "") {
            setEmailErr(true)
        }
        else if (!ProfileBlob) {
            setProfileErr(true)
        }
        else if (Password == "") {
            setPasswordErr(true)
        }
        else {
            var file = new File([ProfileBlob], `${ProfileBlob.lastModifiedDate}.png`);
            const formData = new FormData()
            formData.append("Email", Email)
            formData.append("Password", Password)
            formData.append("Profile", file)

            signup(formData)
                .then(() => window.location = "/login")
                .catch(err => {
                    console.log(err)
                    setEmailErr(true)
                    setPasswordErr(true)
                })
        }

    }

    return (
        <Row style={{ backgroundColor: 'rgb(109, 121, 128)', width: '80%', margin: '10%' }}>
            <Col style={{ marginLeft: '10%', marginTop: '10%', marginBottom: '10%' }}>
                <Row>
                    <img
                        src={logo}
                        style={{ width: '50%', height: '50%' }}
                    />
                </Row>
                <Row>
                    <TextField
                        id="Email"
                        label="Email"
                        value={Email}
                        onChange={(e) => setEmail(e.target.value)}
                        style={{ width: '300px', margin: 10 }}
                        error={emailErr}
                    />
                </Row>
                <Row>
                    <TextField
                        id="Password"
                        label="Password"
                        type="password"
                        style={{ width: '300px', margin: 10 }}
                        value={Password}
                        onChange={(e) => setPassword(e.target.value)}
                        error={passwordErr}
                    />
                </Row>
                <Row style={{ marginLeft: '-1%', marginTop: 10 }}>
                    <Label for="coverGallery">Profile Image</Label>
                </Row>
                <Row style={{ marginLeft: '-1%' }}>
                    <ImgCrop>
                        <Upload
                            name="coverGallery" listType="picture"
                            beforeUpload={(blob) => setProfileBlob(blob)}
                            onChange={(e) => setProfile(e)}
                        >
                            <Button
                                variant="outlined"
                                color="default"
                                startIcon={<CloudUploadIcon />}
                            >Upload</Button>
                        </Upload>
                    </ImgCrop>
                </Row>
                <Row style={{ marginLeft: '0%' }}>
                    <Button style={{ backgroundColor: 'rgb(160, 143, 91)', color: 'white', marginTop: '10%' }} variant="contained" onClick={() => Register()}>Register</Button>
                </Row>
            </Col>
            <Col style={{ marginLeft: '-10%' }}>
                <img
                    src={regis}
                    style={{ width: '100%', height: '100%', position: 'absolute' }}
                />
            </Col>
        </Row>
    )

}
export default Register