import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import { Container, Row, Col, Input, Label, Form } from 'reactstrap';
import { Modal, Upload, Spin } from 'antd';
import 'antd/dist/antd.css';
import ImgCrop from 'antd-img-crop';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusCircle, faUpload, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { postGallery, getAllGallery, getUsersByToken, logout, updateProfileImage, getUserDataById, getAllGallerySort, searchUserByname, host } from '../api';
import { useDispatch } from 'react-redux';
import { isLogOut } from '../actions/userAction'
import logo from '../wonderland.png'
import { TextField, Button, ButtonGroup } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import Collapse from '@material-ui/core/Collapse';

const Home = (props) => {


    const [visible, setVisible] = useState(false)
    const [user, setUser] = useState({})
    const [author, setAuthor] = useState({})
    const [editProfile, setEditProfile] = useState(false)

    const [nameGallery, setNameGallery] = useState("")
    const [coverGallery, setCoverGallery] = useState(null)
    const [listGallery, setListGallery] = useState([])

    const [editImage, setEditImage] = useState(null)
    const [listImageProfile, setListImageProfile] = useState([])

    const token = useSelector(state => state.login.token)
    const user_id = props.match.params.id

    const [friends, setFriends] = useState([])
    const [search, setSearch] = useState("")

    const dispatch = useDispatch()

    const handleOk = () => {
        if (coverGallery == null) {
            alert("Please upload image")
        }
        else if (nameGallery == "") {
            alert("Please input image name")
        }
        else if (coverGallery == null && nameGallery == "") {
            alert("Please input image name and upload image")
        }
        else {
            var file = new File([coverGallery], `${coverGallery.lastModifiedDate}.png`);
            const formData = new FormData()
            formData.append("Name", nameGallery)
            formData.append("Cover", file)
            formData.append("UserID", user.ID)

            createGallery(formData)
            setVisible(false)
        }
    };

    const handleOkImg = () => {
        var file = new File([editImage], `${editImage.name}`);
        const formData = new FormData()
        formData.append("Profile", file)

        updateProfileImg(formData)
        setEditProfile(false)
    };

    const updateProfileImg = (formData) => {
        updateProfileImage(token, user.ID, formData)
            .then(() => getUserData())
            .catch(err => console.log(err))
    }

    const handleCancel = () => {
        setVisible(false)
        setEditProfile(false)
    };

    const addGallery = () => {
        setVisible(true)
    }

    const createGallery = (formData) => {
        postGallery(token, formData)
            .then((response) => window.location = "/gallery/" + response.data.ID)
            .catch(err => console.log(err))
    }

    const fetchGallery = (user_id) => {
        getAllGallery(token, user_id)
            .then(response => {
                setListGallery(response.data)
            })
            .catch(err => console.log(err))
    }

    const getUserData = () => {
        getUsersByToken(token)
            .then((response) => {
                setUser(response.data)
                checkUser(response.data.ID)
            })
            .catch(err => console.log(err))
    }

    const checkUser = (ID) => {
        if (ID == user_id) {
            fetchGallery(ID)
        }
        else {
            console.log('params userid', user_id)
            getUserDataById(token, user_id)
                .then((response) => {
                    setAuthor(response.data)
                    fetchGallery(response.data.ID)
                })
                .catch(err => console.log(err))
        }
    }

    const Search = (value) => {
        if (value == "") {
            setFriends([])
        }
        else {
            searchUserByname(token, value)
                .then(response => {
                    setFriends(response.data)
                })
                .catch(err => console.log(err))
        }

    }

    const setLogout = () => {

        logout(token, user_id)
            .then(() => {
                window.location = "/login"
                dispatch(isLogOut())
            })
            .catch(err => console.log(err))
    }

    useEffect(() => {
        getUserData()
    }, [])


    return (
        <div>
            {
                user == {} ?
                    <div>
                        <Spin />
                    </div>
                    :
                    <Container style={{ height: '72px', alignItems: 'center' }}>
                        <Row xs="3" >
                            <Col style={{ alignSelf: 'center', paddingTop: 24, paddingBottom: 24, alignItems: 'left' }}>
                                <img
                                    onClick={() => window.location = "/list"}
                                    src={logo}
                                    width="250" height="80" />
                            </Col>
                            <Col style={{ alignSelf: 'center', paddingTop: 24, textAlign: 'center', paddingBottom: 24 }}>
                                <Input
                                    style={{ width: '50%', marginLeft: '25%' }}
                                    placeholder="ค้นหา"
                                    onChange={(e) => Search(e.target.value)}
                                    valur={search}
                                />
                                <div style={{ position: 'absolute', width: '50%', zIndex: 1, marginLeft: '23%' }}>
                                    {
                                        friends == [] && search == "" ?
                                            null
                                            :
                                            friends.map(f => {
                                                return (
                                                    <Row
                                                        onClick={() => window.location = `/home/${f.ID}`}
                                                        style={{ marginTop: '5%', width: '90%', marginLeft: '1%', backgroundColor: '#8080807d', borderRadius: 3, padding: 3, alignItems: 'center', paddingLeft: 5 }}>
                                                        <img
                                                            src={`${host}/images/${f.Profile}`}
                                                            style={{ width: 30, height: 30, borderRadius: 50 }}></img>
                                                        <p style={{ marginLeft: '10%', marginTop: '5%' }}>{f.Email}</p>
                                                    </Row>
                                                )
                                            })
                                    }
                                </div>
                            </Col>
                            <Col style={{ paddingRight: '10%', textAlign: 'right', alignSelf: 'center', paddingTop: 24, paddingBottom: 24 }}>
                                <Row>
                                    <img
                                        src={`${host}/images/${user.Profile}`}
                                        style={{ width: 50, height: 50, borderRadius: 50, marginLeft: '100%' }}
                                        onClick={() => window.location = `/home/${user.ID}`} />

                                </Row>
                            </Col>
                        </Row>
                        <hr style={{ marginTop: 0 }} />
                        {/* profile */}
                        <Row xs="2">

                            <Col style={{ paddingLeft: '10%', paddingTop: 24, paddingBottom: 24 }}>
                                <div style={{ width: '75%', backgroundColor: 'antiquewhite', borderRadius: '180px', padding: 10, height: '100%' }}>
                                    {
                                        !author.ID ?
                                            <img
                                                src={`${host}/images/${user.Profile}`}
                                                style={{ width: '100%', height: '300px', backgroundColor: 'pink', borderRadius: '180px' }} />
                                            :
                                            <img
                                                src={`${host}/images/${author.Profile}`}
                                                style={{ width: '100%', height: '300px', backgroundColor: 'pink', borderRadius: '180px' }} />

                                    }

                                </div>
                            </Col>
                            <Col style={{ paddingTop: 24, paddingBottom: 24, alignSelf: 'center', height: 250 }}>
                                <Row >
                                    <Col >
                                        {
                                            !author.ID ?
                                                <h3>{user.Email}</h3>
                                                :
                                                <h3>{author.Email}</h3>
                                        }
                                    </Col>
                                </Row>
                                {
                                    !author.ID ?
                                        <Row style={{ marginTop: 30, marginLeft: '0%' }}>
                                            <Button
                                                variant="outlined"
                                                color="secondary"
                                                onClick={() => setEditProfile(true)}
                                                style={{ height: 40 }}>Change Profile Image</Button>
                                            <FontAwesomeIcon icon={faSignOutAlt} style={{ width: '30%', marginLeft: '-5%', marginTop: '1%' }} size={"2x"} color={"gray"} onClick={setLogout} />
                                        </Row>
                                        :
                                        null

                                }
                            </Col>
                        </Row>
                        {/* listGallery */}
                        <Row xs="4" style={{ marginTop: 30 }}>
                            {
                                !author.ID ?
                                    <Col >
                                        <div style={{ backgroundColor: "#d8d3d1", width: '80%', height: 200, margin: '10%', borderRadius: 120 }} onClick={() => addGallery()} >
                                            <FontAwesomeIcon icon={faPlusCircle} style={{ width: '100%', marginTop: '30%' }} size={"5x"} color={"#7f3c40"} />
                                        </div>
                                    </Col>
                                    :
                                    null

                            }
                            {
                                listGallery.map((item, index) => {
                                    console.log("item", item)
                                    return (
                                        <Col style={{ textAlign: 'center' }} key={item.ID}>
                                            <img
                                                src={`${host}/images/${item.Cover}`}
                                                style={{ width: '80%', height: 200, margin: '10%', borderRadius: 120 }}
                                                onClick={() => { window.location = "/gallery/" + item.ID }}
                                            />
                                            <p>{item.Name}</p>
                                        </Col>
                                    )
                                })
                            }
                        </Row>
                        <Modal
                            title="Add Gallery"
                            visible={visible}
                            onOk={handleOk}
                            onCancel={handleCancel}
                        >
                            <Row>
                                <Col style={{ flex: 1, width: '50%', marginLeft: '25%', marginRight: '25%', flexDirection: 'row' }}>
                                    <Row>
                                        <Label for="nameGallery">Name</Label>
                                        <Input type="text" name="nameGallery" id="nameGallery" placeholder="name gallery" onChange={(e) => setNameGallery(e.target.value)} />
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Row style={{ marginTop: 10 }}>
                                                <Label for="coverGallery">Cover Gallery</Label>
                                            </Row>
                                            <Row>
                                                <ImgCrop>
                                                    <Upload
                                                        name="coverGallery" listType="picture"
                                                        beforeUpload={(blob) => setCoverGallery(blob)}
                                                    // onChange={(e) => setFileList(e)}
                                                    >
                                                        <Button><FontAwesomeIcon icon={faUpload} style={{ marginRight: 5 }} />Click to upload</Button>
                                                    </Upload>
                                                </ImgCrop>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Modal>
                        <Modal
                            title="Edit Profile Image"
                            visible={editProfile}
                            onOk={handleOkImg}
                            onCancel={handleCancel}
                        >
                            <Row>
                                <Col style={{ flex: 1, width: '50%', marginLeft: '25%', marginRight: '25%', flexDirection: 'row' }}>
                                    <Row style={{ marginTop: 10 }}>
                                        <Label for="coverGallery">Profile Image</Label>
                                    </Row>
                                    <Row>
                                        <ImgCrop>
                                            <Upload
                                                name="coverGallery" listType="picture"
                                                beforeUpload={(blob) => setEditImage(blob)}
                                                onChange={(e) => setListImageProfile(e)}
                                            >
                                                <Button><FontAwesomeIcon icon={faUpload} style={{ marginRight: 5 }} />Click to upload</Button>
                                            </Upload>
                                        </ImgCrop>
                                    </Row>
                                </Col>
                            </Row>
                        </Modal>
                    </Container>
            }
        </div>
    );
};
export default Home