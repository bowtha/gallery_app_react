import React, { useState, useEffect } from 'react';
import { Input, Label, Col, Row, } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { login } from '../api';
import { useDispatch } from 'react-redux';
import { isLogin } from '../actions/userAction'
import { Alert } from 'antd'
import { TextField, Button } from '@material-ui/core';
import logo from '../wonderland.png'

const Login = () => {

    const [Email, setEmail] = useState("")
    const [Token, setToken] = useState(null)
    const [Password, setPassword] = useState("")
    const dispatch = useDispatch()
    const [isError, setIsError] = useState(false)
    const setlogin = () => {

        const user = {
            "Email": Email,
            "Password": Password,
        }

        login(user)
            .then((response) => {
                console.log(response.data.token)
                window.location = `/list`
                dispatch(isLogin(response.data.token))
            })
            .catch(err => {
                console.log(err)
                setIsError(true)
            })
        // .catch( 

        //     alert("Email or password not match")
        // )
    }

    return (
        <Row>
            <Col style={{ backgroundColor: 'rgba(114, 130, 136, 0.23)', height: '100%' }}>
                <img src={'https://i.pinimg.com/564x/bd/fe/2f/bdfe2f30656da47b772d44e547221f11.jpg'} style={{ height: '100%', top: '30px' }} />
            </Col>
            <Col style={{ marginTop: '20%', marginBottom: '0', position: 'fixed', marginLeft: '45%', width: '100%' }}>
                <Row>
                    <img
                        src={logo}
                        width="250" height="80" />
                </Row>
                <Row>
                    <TextField
                        id="Email"
                        label="Email"
                        value={Email}
                        onChange={(e) => setEmail(e.target.value)}
                        style={{ width: '300px', margin: 10 }}
                        error={isError}
                    />
                </Row>
                <Row>
                    <TextField
                        id="Password"
                        label="Password"
                        type="password"
                        style={{ width: '300px', margin: 10 }}
                        value={Password}
                        onChange={(e) => setPassword(e.target.value)}
                        error={isError}
                    />
                </Row>
                <Row style={{ marginLeft: '0%' }}>
                    <Button variant="contained" style={{ backgroundColor: 'rgb(206, 145, 81)', color: 'white', marginTop: 20, marginRight: '2%' }} onClick={setlogin}>Login</Button>
                    <Button variant="contained" style={{ backgroundColor: 'rgb(160, 143, 91)', color: 'white', marginTop: 20 }} onClick={() => { window.location = "/register" }}>Register</Button>
                </Row>
            </Col>
        </Row>
    )

}



export default Login
