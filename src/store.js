import { createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

import reducers from './reducers/reducers'

const persistConfig = {
  key: 'root',
  storage: storage
}
const persistedReducer = persistReducer(persistConfig, reducers)

  let store = createStore(persistedReducer)
  let persistor = persistStore(store)

export{ store, persistor }
// export default () => {

//   return 
// }