import React from 'react';
import {  Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

const PrivateRoute = ({ path, component: Component }) => {
    const isLogin = useSelector((state) => state.login.isLogin)
     return(
        <Route path={path} render={(props) => {

            if (isLogin == true) {
                return <Component {...props} /> 
            }
            else {
                return <Redirect to="/login" />
            }
        }} />
    )
}

export default PrivateRoute;