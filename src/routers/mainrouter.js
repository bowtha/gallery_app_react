import React from 'react';
import { BrowserRouter, Route ,Redirect} from 'react-router-dom';
import Home from '../screens/home';
import Gallery from '../screens/gallery';
import Login from '../screens/login';
import Register from '../screens/register';
import PrivateRoute from './privateroute';
import AllImage from '../screens/all_image';


const Main = () => {
    return (
        <BrowserRouter>
            <Route exact path="/" render={() => {return <Redirect to="/login" />}} />
            <PrivateRoute path="/home/:id" component={Home} />
            <PrivateRoute path="/list" component={AllImage} />
            <PrivateRoute path="/gallery/:id" component={Gallery} />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
        </BrowserRouter>
    )
}
export default Main;