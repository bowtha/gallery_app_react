import { LOGIN,LOGOUT } from '../actions/userAction'


const initialState = {
    token: null,
    isLogin:false
}

export const LoginReducer = (state = initialState, action) => {
    switch (action.type) {

        case LOGIN:
            return {
                ...state,
                token: action.token,
                isLogin:true
            }
        case LOGOUT:
            return {
                ...state,
                token: null,
                isLogin:false
            }

        default:
            return state
    }
}
