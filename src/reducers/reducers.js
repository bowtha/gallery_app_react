import { combineReducers } from 'redux'
import { LoginReducer } from './userReducer'


export default  combineReducers({
    login: LoginReducer,
})
